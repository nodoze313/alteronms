Summary: mySQL++ API for mySQL
Name: mysql++
Version: 2.3.2
Release: 1
License: GPL
Group: Applications/Productivity
Source: http://tangentsoft.net/mysql++/releases/mysql++-2.3.2.tar.gz
URL: http://tangentsoft.net/mysql++
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
Distribution: Fedora
Vendor: Tangentsoft
Packager: James MacLachlan <james@pcmsite.net>
Provides: mysqlpp

%description
MySQL++ is a C++ wrapper for MySQL’s C API. It is built around STL principles, to make dealing with the database as easy as dealing with an STL container. MySQL++ relieves the programmer of dealing with cumbersome C data structures, generation of repetitive SQL statements, and manual creation of C++ data structures to mirror the database schema.

%prep

%setup
./configure

%build
make

%install
%makeinstall


%files
%defattr(-,root,root)
/usr/include/mysql++/coldata.h
/usr/include/mysql++/common.h
/usr/include/mysql++/connection.h
/usr/include/mysql++/const_string.h
/usr/include/mysql++/convert.h
/usr/include/mysql++/custom-macros.h
/usr/include/mysql++/custom.h
/usr/include/mysql++/datetime.h
/usr/include/mysql++/exceptions.h
/usr/include/mysql++/field_names.h
/usr/include/mysql++/field_types.h
/usr/include/mysql++/fields.h
/usr/include/mysql++/lockable.h
/usr/include/mysql++/manip.h
/usr/include/mysql++/myset.h
/usr/include/mysql++/mysql++.h
/usr/include/mysql++/noexceptions.h
/usr/include/mysql++/null.h
/usr/include/mysql++/qparms.h
/usr/include/mysql++/query.h
/usr/include/mysql++/querydef.h
/usr/include/mysql++/resiter.h
/usr/include/mysql++/result.h
/usr/include/mysql++/row.h
/usr/include/mysql++/sql_string.h
/usr/include/mysql++/sql_types.h
/usr/include/mysql++/stream2string.h
/usr/include/mysql++/string_util.h
/usr/include/mysql++/tiny_int.h
/usr/include/mysql++/transaction.h
/usr/include/mysql++/type_info.h
/usr/include/mysql++/vallist.h
/usr/lib/libmysqlpp.so
/usr/lib/libmysqlpp.so.2
/usr/lib/libmysqlpp.so.2.3.2
/usr/share/doc/mysql++-2.3.2/COPYING
/usr/share/doc/mysql++-2.3.2/ChangeLog
/usr/share/doc/mysql++-2.3.2/README
/usr/include/mysql++/autoflag.h

%changelog


