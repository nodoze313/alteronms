Summary: Zend Optimizer
Name: zendoptimizer
Version: 1.0
Release: 1
License: GPL
Group: Applications/Productivity
Source: http://www.alteronms.org/zendoptimizer.tar.gz
URL: http://www.alteronms.org/index.html
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
Distribution: Fedora
Vendor: Zend
Packager: James MacLachlan <james@pcmsite.net>

%description
Zend Optimizer

%prep

%setup

%build

%install
make DESTDIR=%buildroot install


%files
#%defattr(-,root,root)
/usr/local/Zend/bin
/usr/local/Zend/lib/Optimizer-3.3.3/php-5.2.x/ZendOptimizer.so
/usr/local/Zend/lib/Optimizer_TS-3.3.3/php-5.2.x/ZendOptimizer.so
/usr/local/Zend/lib/ZendExtensionManager.so
/usr/local/Zend/lib/ZendExtensionManager_TS.so
/usr/local/Zend/doc
/usr/local/Zend/etc
/usr/local/Zend/lib
/usr/local/Zend/logs

%changelog


