Summary: Point of sale KDE client for a number of backends.
Name: alteropos
Version: 1.0
Release: 1
License: GPL
Group: Applications/Productivity
Source: http://www.alteronms.org/alteropos.tar.gz
URL: http://www.alteronms.org/index.html
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
Distribution: Fedora
Vendor: Altero LLC
Packager: James MacLachlan <james@pcmsite.net>
Provides: pcmpos libposdata.so

%description
Altero POS is a GPL point of sale client that works with NolaPro.

%prep

%setup
./configure

%build
make

%install
%makeinstall


%files
%defattr(-,root,root)
%doc README TODO COPYING ChangeLog
/usr/share/applnk/Utilities/alteropos.desktop
/usr/lib/libposdata.la
/usr/lib/libposdata.so
/usr/lib/libposdata.so.0
/usr/lib/libposdata.so.0.0.0
%dir /usr/share/apps/alteropos
%dir /usr/share/doc/HTML/en/pcmpos
/usr/share/icons/hicolor/16x16/apps/pcmpos.png
/usr/share/icons/hicolor/32x32/apps/pcmpos.png
/usr/share/doc/HTML/en/pcmpos/common
/usr/share/doc/HTML/en/pcmpos/index.cache.bz2
/usr/share/doc/HTML/en/pcmpos/index.docbook
/usr/bin/alteropos
/usr/share/applnk/Utilities/alteropos_startmenu.desktop
/usr/share/apps/alteropos/alteroposui.rc

%changelog


