/***************************************************************************
 *   Copyright (C) 2008 by James MacLachlan   *
 *   james@pcmsite.net   *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/


#ifndef _ALTEROBOOKSPREF_H_
#define _ALTEROBOOKSPREF_H_

#include <kdialogbase.h>
#include <qframe.h>

class alterobooksPrefPageOne;
class alterobooksPrefPageTwo;

class alterobooksPreferences : public KDialogBase
{
    Q_OBJECT
public:
    alterobooksPreferences();

private:
    alterobooksPrefPageOne *m_pageOne;
    alterobooksPrefPageTwo *m_pageTwo;
};

class alterobooksPrefPageOne : public QFrame
{
    Q_OBJECT
public:
    alterobooksPrefPageOne(QWidget *parent = 0);
};

class alterobooksPrefPageTwo : public QFrame
{
    Q_OBJECT
public:
    alterobooksPrefPageTwo(QWidget *parent = 0);
};

#endif // _ALTEROBOOKSPREF_H_
