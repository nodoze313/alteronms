/*
libnolapro.cpp - Integration to Nolapro backend.

Copyright (c) 2008 by James MacLachlan and Phoenix Computer Management, LLC

This file is part of PCM POS.

PCM POS is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

PCM POS is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with PCM POS.  If not, see <http://www.gnu.org/licenses/>. 
*/

#include "libnolapro.h"

NolaPro::NolaPro(const char* Database,const char* dbhost,const char* CompanyID,const char* CustomerID)
{
  database = Database;
  host = dbhost;
  user = "";
  pass = "";
  companyID = CompanyID;
  customerID = CustomerID;
  userID = "";

  //create a new mysqlpp instance
  con = new mysqlpp::Connection(false);

  //start the random number generator
  srand(time(0));
#ifdef DEBUG
  //printf("arCash: %s\narChecking: %s\ninvCash: %s\ninvSales: %s\ninvTax: %s\ntaxRate: %f\n",arCash.c_str(),arChecking.c_str(),invCash.c_str(),invSales.c_str(),invTax.c_str(),taxRate);
#endif
}

NolaPro::~NolaPro()
{
}

int NolaPro::databaseConnect()
{
  con->connect(database,host,user.c_str(),pass.c_str());
  if(con->connected())
    return NO_ERROR;
  else
  {
#ifdef DEBUG
    printf("MySQL Error: %s\n",con->error());
#endif
    return GENERAL_ERROR;
  }
}


mysqlpp::Result NolaPro::doQuery(string Query)
{
#ifdef DEBUG
  printf("Query: %s\n\n",Query.c_str());
#endif
  if(!con->connected())
  {
    databaseConnect();
  }
  mysqlpp::Query query = con->query();
  query << Query.c_str();
  return query.store();
}

Item* NolaPro::getItemByUPC(string UPC)
{
  if(databaseConnect())
    return false;
  if(con->success())
  {
    string queryString = "SELECT id,itemcode,description,catalogdescription,price,taxexempt,products_quantity,products_costprice FROM `item` WHERE ";
    //if the length of the code is 12 or 13 look up UPC(EAN)
    if(UPC.length() == 12 || UPC.length() == 13)
    {
      queryString += "upc = '";
      queryString += UPC;
      queryString += "'";
    }
    else  //if other length lookup by itemcode
    {
      queryString += "itemcode = '";
      queryString += UPC;
      queryString += "'";
    }
    mysqlpp::Result res = doQuery(queryString);
    LastResult = &res;

    //create a Item
    Item* item = new Item();
    if(res.num_rows() > 0)
    {
      //get row from result
      mysqlpp::Row itemRow = LastResult->fetch_row();
      item->ID = itemRow.raw_string(0);
      item->ItemCode = itemRow.raw_string(1);
      item->Description = itemRow.raw_string(2);
      item->LongDescription = itemRow.raw_string(3);
      //item->Price = stof(itemRow.raw_string(4));
      item->TaxFlag = stob(itemRow.raw_string(5));
      item->Available = atoi(itemRow.raw_string(6).c_str());
      item->Cost = itemRow.raw_string(7);
      //get the price of the item
      queryString = "SELECT priceperpriceunit.price FROM priceperpriceunit WHERE priceperpriceunit.itemid='";
      queryString += item->ID;
      queryString += "' AND priceperpriceunit.pricelevelid='1' AND itemlocationid='1'"; //as qty's increase this will be important
      mysqlpp::Result ppures = doQuery(queryString);
      if(ppures.num_rows() > 0)
      {
        mysqlpp::Row itemRowp = ppures.fetch_row();
        printf("\n\nResult: %s\n\n",itemRowp.raw_string(0).c_str());
        item->Price = stof(itemRowp.raw_string(0));
      }
      else
      {
        printf("No result\n");
      }
      con->close();
      return item;
    }
    else
    {
#ifdef DEBUG
      printf("No valid items: %s\n", item->ID.c_str());
#endif
    }
  }
  return new Item(false);
}

int NolaPro::updateInventory(Item* Item, signed int Quantity)
{
  if(databaseConnect())
    return false;
  //calculate the number after transaction
  signed int NewQuantity = Quantity + Item->Available;
  if(NewQuantity < 0)
    NewQuantity = 0;
  //store new total
  string queryString = "UPDATE `item` SET products_quantity = '";
  char* buffer = new char[128];
  sprintf(buffer, "%d", NewQuantity);
  queryString += buffer;
  queryString += "' WHERE id = '";
  queryString += Item->ID;
  queryString += "'";
  doQuery(queryString);
  return NO_ERROR; 
}

int NolaPro::completeSale(PAYMENT_METHOD PM, string Total,string Tax,map<string,Item> &Items)
{
  if(PM == CREDITCARD)
    insertCreditCardPayment(Total,Tax);
  insertARInvoice(Total,Tax);
  getARInvoiceID();
  for(ITEMS::iterator it = Items.begin();it != Items.end();it++)
  {
    insertARInvoiceDetail(Total,&it->second);
    //Item p = *it;
    //p.ShowProduct();
    insertItemTransaction(&it->second);
  }
  insertARInvoiceDetailPaymentMethod(PM,itoa(Items.size()+1));
  insertGLTransVoucher();
  insertARInvoicePaymentDetail(Total,PM);
  string VoucherID = getGLTransVoucher();

  insertGLTransaction(PM,VoucherID,string(sps(Total,Tax)));
  insertGLTransVoucher();
  VoucherID = getGLTransVoucher();
  string ITotal = "-";
  ITotal += Total;
  insertGLTransaction(NONE,VoucherID,ITotal,(char*)invSales.c_str());
  string ITax = "-";
  ITax += Tax;
  insertGLTransaction(NONE,VoucherID,ITax,(char*)taxAcct.c_str());
  insertGLTransaction(NONE,VoucherID,sps(Total,Tax),(char*)arReceivables.c_str());
  updateARInvoice();
  return NO_ERROR;
}

POS_ERRORS NolaPro::loginUser(string username,string password)
{
  user = username;
  pass = password;
  
  //log user into the system
  string QueryString = "SELECT id FROM `genuser` WHERE name = '";
  QueryString += username;
  QueryString += "' AND (password = '";
  QueryString += password;
  QueryString += "' OR password = MD5('";
  QueryString += password;
  QueryString += "')) AND cancel=0 AND active=1";
  mysqlpp::Result res = doQuery(QueryString);
  if(res.num_rows() > 0)
  {
    mysqlpp::Row itemRow = res.fetch_row();
    userID = itemRow.raw_string(0); //userID
  }
  else
  {
    return INVALID_AUTH;
  }
  
  //get company info
  getCompanyARInfo();
  getCompanyInvInfo();
  getSalesTaxRate();
  return VALID_AUTH;
}

POS_ERRORS NolaPro::logoutUser()
{
  string QueryString = "update genuserhome set lastlogin=NOW() where genuser='";
  QueryString += userID;
  QueryString += "' and gencompany='";
  QueryString += companyID;
  QueryString += "'";
  doQuery(QueryString);

  //clear username and password for security
  user = "";
  pass = "";
  return NO_ERROR;
}

int NolaPro::getCompanyARInfo()
{
  if(databaseConnect())
    return false;
  string QueryString = "SELECT cash,checking,receivables,interest,discount,depositglacctid,cost FROM arcompany WHERE id='";
  QueryString += companyID;
  QueryString += "'";
  
  mysqlpp::Result res = doQuery(QueryString);
  //create a Item
  Item* item = new Item();
  if(res.num_rows() > 0)
  {
    //get row from result
    mysqlpp::Row itemRow = res.fetch_row();
    arCash = itemRow.raw_string(0);
    arChecking = itemRow.raw_string(1);
    arReceivables = itemRow.raw_string(2);
    
    return NO_ERROR;
  }
  else
    return GENERAL_ERROR;
}

int NolaPro::getCompanyInvInfo()
{
  if(databaseConnect())
    return false;
  string QueryString = "SELECT * FROM invcompany WHERE id='";
  QueryString += companyID;
  QueryString += "'";
  mysqlpp::Result res = doQuery(QueryString);
  //create a Item
  Item* item = new Item();
  if(res.num_rows() > 0)
  {
    //get row from result
    mysqlpp::Row itemRow = res.fetch_row();
    invCash = itemRow.raw_string(1);
    invSales = itemRow.raw_string(2);
    costGLAccount = itemRow.raw_string(4);
    invTax = itemRow.raw_string(6);
    return NO_ERROR;
  }
  else
    return GENERAL_ERROR;
}

int NolaPro::getSalesTaxRate()
{
  if(databaseConnect())
    return false;
  string QueryString = "SELECT salestax.id,salestax.taxrate,salestax.glacctid FROM salestax,pickupsalestax WHERE pickupsalestax.salestaxid=salestax.id AND pickupsalestax.gencompanyid='";
  QueryString += companyID;
  QueryString += "'";
  QueryString += "AND salestax.cancel=0";
  mysqlpp::Result res = doQuery(QueryString);
  //create a Item
  Item* item = new Item();
  if(res.num_rows() > 0)
  {
    //get row from result
    mysqlpp::Row itemRow = res.fetch_row();
    taxID = itemRow.raw_string(0);
    taxRate = atoi(itemRow.raw_string(1).c_str());
    taxAcct = itemRow.raw_string(2);
    taxRate = (taxRate + 0.0) * 0.01;
    return NO_ERROR;
  }
  else
    return GENERAL_ERROR;
}

int NolaPro::getNextInvoiceNumber()  //increment the value here, this is part an update method
{
  if(databaseConnect())
    return false;
  string QueryString = "SELECT nextinvoicenum FROM arcompany WHERE id='";
  QueryString += companyID;
  QueryString += "'";
  mysqlpp::Result res = doQuery(QueryString);
  if(res.num_rows() > 0)
  {
    //get row from result
    mysqlpp::Row itemRow = res.fetch_row();
    currentInvoiceNum = atoi(itemRow.raw_string(0).c_str());
    //icrement the nextinvoicenum
    QueryString =  "UPDATE arcompany SET nextinvoicenum='";
    QueryString += itoa(currentInvoiceNum + 1);
    QueryString += "' WHERE id='";
    QueryString += companyID;
    QueryString += "' AND nextinvoicenum<'";
    QueryString += itoa(currentInvoiceNum + 1);
    QueryString += "'";
    //error handeling needed here
    doQuery(QueryString);
    return NO_ERROR;
  }
  else
    return GENERAL_ERROR;
}
int NolaPro::getARInvoiceID()
{
  string QueryString = "SELECT id FROM arinvoice WHERE payplan=0 AND status='-1' AND invoicenumber='";
  QueryString += itoa(currentInvoiceNum);
  QueryString += "' AND gencompanyid='";
  QueryString += companyID;
  QueryString += "' AND entryuserid='";
  QueryString += userID;
  QueryString += "' ORDER BY id DESC LIMIT 1";
  mysqlpp::Result res = doQuery(QueryString);
  if(res.num_rows() > 0)
  {
    //get row from result
    mysqlpp::Row itemRow = res.fetch_row();
    currentARInvoiceID = itemRow.raw_string(0);
  }
  else
  {
    printf("No records matched, here is the query:\n %s\n",QueryString.c_str());
  }
  return NO_ERROR;
}

string NolaPro::getGLTransVoucher()
{
  string QueryString = "SELECT max(id) FROM gltransvoucher WHERE wherefrom='2' AND entryuserid='";
  QueryString += userID;
  QueryString += "'";
  mysqlpp::Result res = doQuery(QueryString);
  if(res.num_rows() > 0)
  {
    //get row from result
    mysqlpp::Row itemRow = res.fetch_row();
    return itemRow.raw_string(0);
  }
  string err = "00";
  return err;
}

string NolaPro::getCCTransactionCurrent()
{
  string QueryString = "SELECT max(id) FROM `cctransaction`;";
  string id = "0";
  mysqlpp::Result res = doQuery(QueryString);
  if(res.num_rows() > 0)
  {
    //get row from result
    mysqlpp::Row itemRow = res.fetch_row();
    id = itemRow.raw_string(0);
  }
  return id;
}

int NolaPro::insertCreditCardPayment(string Total, string Tax)
{
  string QueryString = "INSERT INTO cctransaction SET entrydate=NOW() , resultcode='A' , resultreasoncode='' , resultreasontext='' , authcode='' , transid='' , amount='";
  QueryString += Total;
  QueryString += "' , cardfirstname='POS' , cardlastname='Sale' , transtype='S' , refertable='gltransvoucher' , referid='0' , nontaxable='0' , tax='";
  QueryString += Tax;
  QueryString += "' , shipping='0' , customerip='127.0.0.1' , customerid='";
  QueryString += customerID;
  QueryString += "' , ccaccountid='1' , cardnumber='' , cardexp='' , cardtype='' , entryuserid='";
  QueryString += userID;
  QueryString += "' , b2buserid='' , customerconfirmation='";
  QueryString += randString();
  QueryString += "'";
  doQuery(QueryString);
  return NO_ERROR;
}

int NolaPro::insertCashPayment()
{
  //this doesn't look like it is required
  return NO_ERROR;
}

int NolaPro::insertCheckingPayment()
{
  //this doesn't look like it is required
  return NO_ERROR;
}

int NolaPro::insertARInvoice(string Total, string Tax)
{
  getNextInvoiceNumber();
  string QueryString = "INSERT INTO arinvoice (invoicenumber,ponumber,shipping_method,sales_categoriesid,wherefrom,orderbycompanyid,shiptocompanyid,status,customerbillcode,invoicetermsid,salesmanid,datelastinterestcalc,gencompanyid,entrydate,entryuserid,lastchangeuserid,invoicetotal,invoicedate) VALUES ('";
  QueryString += itoa(currentInvoiceNum);
  QueryString += "', '', '', '', '2', '";
  QueryString += customerID;
  QueryString += "', '0', -1, '', '','";
  QueryString += userID;
  QueryString += "', NOW(), '";
  QueryString += companyID;
  QueryString += "', NOW(), '";
  QueryString += userID;
  QueryString += "', '";
  QueryString += userID;
  QueryString += "','";
  QueryString += sps(Total,Tax);
  QueryString += "',NOW())";
  doQuery(QueryString);
  return NO_ERROR;
}

int NolaPro::insertARInvoiceTaxDetail(string Total, string Tax)
{
  string QueryString = "INSERT INTO arinvoicetaxdetail(invoiceid,taxid,taxamount) values (";
  QueryString += itoa(currentInvoiceNum);
  QueryString += ", ";
  QueryString += taxID;
  QueryString += ", ";
  QueryString += Tax;
  QueryString += ")";
  doQuery(QueryString);
  return NO_ERROR;
}

int NolaPro::insertARInvoiceDetail(string Total, Item* item)
{
  string QueryString = "INSERT INTO arinvoicedetail(invoiceid,linenumber,itemid,description,qty,                                                qtyunitnameid,glaccountid,taxflag,priceach,priceunitnameid,qtyunitperpriceunit,totalprice, entrydate,entryuserid,lastchangeuserid,costeach,costglaccountid,taxflag2,taxflag3,taxflagid,taxflag2id,taxflag3id,orig_priceach,discount_amount,discount_percent) VALUES (";
  QueryString += currentARInvoiceID;
  QueryString += ",";
  QueryString += item->LineNumber;
  QueryString += ",";
  QueryString += item->ID;
  QueryString += ",'";
  QueryString += item->Description;
  QueryString += "','";
  QueryString += item->Quantity;
  QueryString += "',0,'";
  QueryString += invSales;
  QueryString += "',";
  QueryString += item->TaxFlag;
  QueryString += ",'";
  QueryString += ftoa(item->Price);
  QueryString += "',0,1,";//quantityunitperpriceunit needs to be edited
  QueryString += "'";
  QueryString += ftoa(item->SubTotal());
  QueryString += "', NOW(),";
  QueryString += userID;
  QueryString += ",";
  QueryString += userID;
  QueryString += ",'','0',";
  QueryString += item->TaxFlag;
  QueryString += ",";
  QueryString += item->TaxFlag;
  QueryString += ",0,0,0,'";
  QueryString += ftoa(item->Price);
  "',0,0)";
  doQuery(QueryString);
  return NO_ERROR;
}
int NolaPro::insertARInvoiceDetailPaymentMethod(PAYMENT_METHOD pm, string LineNumber)
{
  string QueryString = "insert into arinvoicedetail(invoiceid,linenumber,itemid,description,qty, qtyunitnameid,glaccountid,taxflag,priceach,priceunitnameid,qtyunitperpriceunit,totalprice,entrydate,entryuserid,lastchangeuserid,costeach,costglaccountid,taxflag2,taxflag3,taxflagid,taxflag2id,taxflag3id,orig_priceach,discount_amount,discount_percent) values (20,";
  QueryString += LineNumber;
  QueryString += ",0,'";
  switch(pm)
  {
    case CASH:
      QueryString += "Paid Cash";
      break;
    case CREDITCARD:
      QueryString += "Paid Credit Card: ";
      QueryString += getCCTransactionCurrent();
      break;
    case CHECK:
      QueryString += "Paid Check";
      break;
  }
  QueryString += "',0,0,'',1,0,'',1,0,NOW(),";
  QueryString += userID;
  QueryString += ",";
  QueryString += userID;
  QueryString += ",0,'";
  QueryString += costGLAccount;
  QueryString += "',1,1,0,0,0,0,0,0)";
  doQuery(QueryString);
  return NO_ERROR;
}
int NolaPro::insertGLTransVoucher()
{
  string QueryString = "INSERT INTO gltransvoucher (voucher,description,wherefrom,companyid,entrydate,lastchangeuserid,entryuserid,lastchangedate) VALUES ('";
  QueryString += itoa(currentInvoiceNum);
  QueryString += "', 'AR Invoice Payment','2', '";
  QueryString += companyID;
  QueryString += "', NOW(), '";
  QueryString += userID;
  QueryString += "', '";
  QueryString += userID;
  QueryString += "',NOW())";
  doQuery(QueryString);
  return NO_ERROR;
}

int NolaPro::insertARInvoicePaymentDetail(string Total,PAYMENT_METHOD pm)
{
  string QueryString = "INSERT INTO arinvoicepaymentdetail (invoiceid, amount, voucherid, datereceived, paymeth, interest,checkid) VALUES ('";
  QueryString += currentARInvoiceID;
  QueryString += "', '";
  QueryString += Total;
  QueryString += "', '";
  QueryString += getGLTransVoucher();
  QueryString += "', NOW(), '";
  QueryString += itoa(pm);
  QueryString += "',0,'";
  QueryString += getCCTransactionCurrent();
  QueryString += "')";
  doQuery(QueryString);
  return NO_ERROR;
}

string GLQueryString(string AccountID,string VoucherID, string Amount)
{
  string QueryString = "INSERT INTO gltransaction (glaccountid,voucherid,amount) VALUES ('";
  QueryString += AccountID;
  QueryString += "', ";
  QueryString += VoucherID;
  QueryString += ", '";
  QueryString += Amount;
  QueryString += "')";
  return QueryString;
}

//needs to support tax account too!
int NolaPro::insertGLTransaction(PAYMENT_METHOD pm, string VoucherID, string Amount, char* Account)
{
  string iAmount = "";
  switch(pm)
  {
    case CASH:
      doQuery(GLQueryString(arCash,VoucherID,Amount));
      iAmount = "-";
      iAmount += Amount;
      doQuery(GLQueryString(arReceivables,VoucherID,iAmount));
      break;
    case CHECK:
      doQuery(GLQueryString(arChecking,VoucherID,Amount));
      iAmount = "-";
      iAmount += Amount;
      doQuery(GLQueryString(arReceivables,VoucherID,iAmount));
      break;
    case CREDITCARD:
      doQuery(GLQueryString(arChecking,VoucherID,Amount));
      iAmount = "-";
      iAmount += Amount;
      doQuery(GLQueryString(arReceivables,VoucherID,iAmount));
      break;
    default:
      doQuery(GLQueryString(Account,VoucherID,Amount));
  }
  return NO_ERROR;
}

int NolaPro::updateARInvoice()
{
  string QueryString = "UPDATE arinvoice SET status=2,discountamount=0,discountdate=NOW(), lastchangeuserid='";
  QueryString += userID;
  QueryString += "' where id='";
  QueryString += currentARInvoiceID;//this is wrong
  QueryString += "'";
  doQuery(QueryString);
  return NO_ERROR;
}

int NolaPro::insertItemTransaction(Item* Item)
{
  if(!updateItemTransactionSequence())
  {
    //get the max id from itemtransaction
    string QueryString = "SELECT max(id) FROM itemtransactionseq";
    string ID;
    mysqlpp::Result res = doQuery(QueryString);
    if(res.num_rows() > 0)
    {
      //get row from result
      mysqlpp::Row itemRow = res.fetch_row();
      ID = itemRow.raw_string(0);
    }
    //Item->ShowProduct();
    QueryString = "INSERT INTO itemtransaction (id,itemid,wherefrom,referenceid,locationid,description,quantity,costpereach,pricepereach,trtype,createdate,createuserid) VALUES (";
    QueryString += ID;
    QueryString += ",'";
    QueryString += Item->ID;
    QueryString += "',2,'";
    QueryString += itoa(currentInvoiceNum);
    QueryString += "','1','used for POS order ";
    QueryString += itoa(currentInvoiceNum);
    QueryString += "','";
    QueryString += itoa(Item->Quantity);
    QueryString += "', '";
    QueryString += Item->Cost;//cost ea
    QueryString += "', '";
    QueryString += ftoa(Item->Price);//price ea
    QueryString += "',3,NOW(),'";
    QueryString += userID;
    QueryString += "')";
    doQuery(QueryString);
  }
  return NO_ERROR;
}

int NolaPro::updateItemTransactionSequence()
{
  string QueryString = "update itemtransactionseq set id=LAST_INSERT_ID(id+1)";
  doQuery(QueryString);
  return NO_ERROR;
}

int NolaPro::CompleteTransaction(PAYMENT_METHOD PM)
{
  return NO_ERROR;
}

float stof(string s)
{
  //find the decimal
  int dp = s.find(".");
  //break the string in half
  string ipart = s.substr(0,dp);
  string dpart = s.substr(dp+1);
  //put the integer portion in the float
  int i = atoi(ipart.c_str());
  //
  float fdpart = atoi(dpart.c_str());
  fdpart = fdpart/100.0;
  //add the decimal to the float
  float out = i + fdpart;
  return out;
}

bool stob(string s)
{
  int b = atoi(s.c_str());
  if(b == 1)
    return true;
  else
    return false;
}

char* itoa(int i)
{
  char* buffer = new char[(sizeof(int)*sizeof(int)+1)];
  int len = sprintf(buffer,"%i\0",i);
  return buffer;
}

char* utoa(unsigned int i)
{
  char* buffer = new char[(sizeof(int)*sizeof(int)+1)];
  int len = sprintf(buffer,"%u\0",i);
  return buffer;
}

char* ftoa(float f)
{
  char* buffer = new char[(sizeof(float)*sizeof(float)+1)];
  int len = sprintf(buffer,"%03.2f\0",f);
  return buffer;
}

char* sps(string a, string b)
{
  float A = stof(a);
  float B = stof(b);
  return ftoa(A+B);
}

char* randString()
{
  FILE* f = fopen("/dev/random","rb");
  unsigned int r;
  int count = fread(&r,sizeof(unsigned int),1,f);
  char* rs = utoa(r);
  char* m = new char[36];
  //this will not link for some reason, not imparitive the randomness is pretty good w/o it
  //md5_buffer(rs,strlen(rs), m);
  //return m;
  return rs;
}

