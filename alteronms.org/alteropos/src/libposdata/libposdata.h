/*
libposdata.h - Definitions for libposdata

Copyright (c) 2008 by James MacLachlan and Phoenix Computer Management, LLC

This file is part of PCM POS.

PCM POS is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

PCM POS is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with PCM POS.  If not, see <http://www.gnu.org/licenses/>. 
*/
#ifndef libposdata_h
#define libposdata_h

#include "libnolapro.h"
#include "pcm_errors.h"

#include <vector>
#include <string>

using namespace std;

enum ACTION
{
  UPDATE,
  SET
};

class Register : public NolaPro
{
public:
  Register(const char* Database,const char* DBHost,const char* CompanyID,const char* CustomerID);
  ITEMS LineItems;
  void GetTotals(float &Total, float &Tax);  //total tax and total of items
  POS_ERRORS ManageItem(string UPC, signed int Count, ACTION action = UPDATE);//used to add and remove count items, if 0 items exist the item will be removed
  int CompleteTransaction(PAYMENT_METHOD PM);
  void CancelSale();
  void CreateLists();
  void ClearLists();
  
  POS_ERRORS UserLogin(string username, string password);
  POS_ERRORS UserLogout();
  
  string UPCList;
  string DescriptionList;
  string QuantityList;
  string PriceList;
  string TotalList;
  //for printing receipt
  string ItemsByRows;
  string SubTotalRow;
  string TaxRow;
  string TotalRow;
};



#endif
