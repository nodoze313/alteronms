/*
libposdata.cpp - Containers for local data when GUI is open, classes do data calcualtions.

Copyright (c) 2008 by James MacLachlan and Phoenix Computer Management, LLC

This file is part of PCM POS.

PCM POS is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

PCM POS is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with PCM POS.  If not, see <http://www.gnu.org/licenses/>. 
*/

#include "libposdata.h"

Register::Register(const char* Database,const char* DBHost,const char* CompanyID,const char* CustomerID) : NolaPro(Database,DBHost,CompanyID,CustomerID)
{
}

void Register::GetTotals(float &Total, float &Tax)
{
  for(ITEMS::iterator it = LineItems.begin();it != LineItems.end();it++)
  {
    float st = it->second.SubTotal();
    Total += st;
    Tax += (st * taxRate);
  }
}

POS_ERRORS Register::ManageItem(string GenericCode, signed int Count,ACTION action)
{
  //look at map for item
  Item* tItem = getItemByUPC(GenericCode);
  ITEMS::iterator it = LineItems.find(tItem->ItemCode);
  if(it == LineItems.end())
  {
    //if not found, add new item
    printf("The item was not on the list ... adding it now.\n");
    if(tItem->ItemIsValid)
    {
      it = LineItems.insert(LineItems.end(),make_pair(tItem->ItemCode,*tItem));
    }
    else
    {
      return INVALID_UPC;
    }
  }
  Item* item = &it->second;
  
  if(action == SET)
  {
    item->Quantity = Count;
  }
  else if(action == UPDATE)
  {
    item->Quantity += Count;
  }
  
  if(item->Quantity <= 0)
  {
    //delete item
    LineItems.erase(it);
  }
  return NO_ERROR;
}

int Register::CompleteTransaction(PAYMENT_METHOD PM)
{
  float Total,Tax;
  GetTotals(Total,Tax);
  int r =  completeSale(PM,ftoa(Total),ftoa(Tax),LineItems);
  ClearLists();
  LineItems.clear();
  return r;
}

void Register::CreateLists()
{
  ClearLists();
  for(ITEMS::iterator it = LineItems.begin();it != LineItems.end();it++)
  {
    //create seperate lists
    UPCList += it->first;
    UPCList += "\n";

    DescriptionList += it->second.Description;
    DescriptionList += "\n";

    QuantityList += itoa(it->second.Quantity);
    QuantityList += "\n";

    PriceList += ftoa(it->second.Price);
    PriceList += "\n";

    TotalList += ftoa(it->second.SubTotal());
    TotalList += "\n";
#ifdef DEBUG
    it->second.ShowProduct();
#endif
    //create printing list
    string sDesc = it->second.Description.substr(0,20);
    if(sDesc.length() > 20)
    {
      while(sDesc.length() < 20)
        sDesc += " ";
    }
    
    ItemsByRows += sDesc;
    ItemsByRows += "  ";
    ItemsByRows += itoa(it->second.Quantity);
    ItemsByRows += "   $";
    ItemsByRows += ftoa(it->second.SubTotal());
    ItemsByRows += "\n";
  }
  float fSubtotal=0.0,fTotal=0.0,fTax=0.0;
  GetTotals(fSubtotal,fTax);
  fTotal = fSubtotal + fTax;
  SubTotalRow = "Subtotal:     ";
  SubTotalRow += ftoa(fSubtotal);
  TaxRow = "Tax:          ";
  TaxRow += ftoa(fTax);
  TotalRow = "Total:        ";
  TotalRow += ftoa(fTotal);
}

void Register::ClearLists()
{
  UPCList = "";
  DescriptionList = "";
  QuantityList = "";
  PriceList = "";
  TotalList = "";
  ItemsByRows = "Desc.                Qty   Total\n";
}

void Register::CancelSale()
{
  ClearLists();
  LineItems.clear();
}

POS_ERRORS Register::UserLogin(string username, string password)
{
  printf("Username: %s  Password: %s\n",username.c_str(),password.c_str());
  return loginUser(username, password);
}

POS_ERRORS Register::UserLogout()
{
  return logoutUser();
}



