#ifndef _PCM_ERRORS_H
#define _PCM_ERRORS_H

enum POS_ERRORS
{
  NO_ERROR,
  GENERAL_ERROR,
  INVALID_UPC,
  INVALID_AUTH,
  VALID_AUTH
};


#define LINE printf("FILE: %s, LINE: %i\n",__FILE__,__LINE__);

#endif



