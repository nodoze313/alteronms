/*
libnolapro.h - Definitions for libnolapro

Copyright (c) 2008 by James MacLachlan and Phoenix Computer Management, LLC

This file is part of PCM POS.

PCM POS is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

PCM POS is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with PCM POS.  If not, see <http://www.gnu.org/licenses/>. 
*/

#ifndef libnolapro_h
#define libnolapro_h

#include <mysql++/mysql++.h>

#include <md5.h>

#include <stdio.h>
#include <stdlib.h>

#include <vector>
#include <string>

using namespace std;

#include "pcm_errors.h"

#define ITEMS map<string,Item>

enum PAYMENT_METHOD
{
  NONE,
  CASH,
  CHECK,
  CREDITCARD
};

float stof(string s);
bool stob(string s);
char* randString();
char* itoa(int i);
char* utoa(unsigned int i);
char* ftoa(float f);
char* sps(string a, string b);

class Item
{
public:
  Item(bool valid=true){ItemIsValid = valid; Quantity = 0;};
  string ID; //id used in database
  string ItemCode; //itemcode
  string Description; //description
  string LongDescription; //catalogdescription
  string Cost;  //our cost of an item
  float Price; //price before tax
  int TaxFlag;  //1 for taxable, 0 non taxable
  signed int Quantity; //number of items ordered
  signed int Available; //products_quantity
  string LineNumber;
  bool ItemIsValid;
  float SubTotal(){return Price*(Quantity*1.0);}; //total of items before tax
#ifdef DEBUG
  void ShowProduct(char* Comment = ""){printf("%s\nID: %s\nUPC: %s\nDesc: %s\nLDesc: %s\nPrice: %f\nTaxFlag: %i\nQty: %i\nAvail: %i\nLine: %s\nSubTotal: %f\n",Comment,ID.c_str(),ItemCode.c_str(),Description.c_str(),LongDescription.c_str(),Price,TaxFlag,Quantity,Available,LineNumber.c_str(),SubTotal());};
#endif
  float Total(float TaxRate){return (SubTotal()*TaxRate)+SubTotal();}
  float Tax(float TaxRate){return SubTotal()*TaxRate;}
};

class NolaPro
{
  const char* database; //config
  string user; //entered on button click
  //this should be secured in memory so that spyware cannot steal it
  string pass; //entered on button click
  const char* host; //config
  const char* companyID;  //config
  const char* customerID; //config, this should be the generic customer for the cash register
  string userID; //this is returned when the user logs in
  
  string arCash;  //database arcompany
  string arChecking;  //database arcompany
  string arReceivables; //database arcompany
  
  string invCash;
  string invSales;
  string invTax;

  string costGLAccount;
  
  string taxID;
  string taxAcct;
  
  int currentInvoiceNum;
  string currentARInvoiceID;
  
  //database select methods
  int getCompanyARInfo();
  int getCompanyInvInfo();
  int getSalesTaxRate();
  int getNextInvoiceNumber();  //increment the value here, this is part an update method
  int getARInvoiceID();
  string getGLTransVoucher();
  
  //database update and insert methods
  int insertCreditCardPayment(string Total, string Tax);
  int insertCashPayment();
  int insertCheckingPayment();
  int insertARInvoice(string Total,string Tax);
  int insertARInvoiceDetail(string Total, Item* item);
  int insertARInvoiceDetailPaymentMethod(PAYMENT_METHOD pm, string LineNumber);
  int insertARInvoiceTaxDetail(string Total, string Tax);
  int insertGLTransVoucher();
  int insertARInvoicePaymentDetail(string Total,PAYMENT_METHOD pm);
  int insertGLTransaction(PAYMENT_METHOD pm, string VoucherID, string Amount,char* Account = "000" );
  int insertItemTransaction(Item* Item);
  int updateARInvoice();
  int updateItemTransactionSequence();

  mysqlpp::Connection* con;

  string getCCTransactionCurrent();

public:
  NolaPro(const char* Database,const char* dbhost,const char* CompanyID,const char* CustomerID);
  virtual ~NolaPro();
  float taxRate;
  int databaseConnect();
  mysqlpp::Result doQuery(string Query);
  Item* getItemByUPC(string UPC);
  int updateInventory(Item* Item, signed int Quantity);
  int completeSale(PAYMENT_METHOD PM, string Total,string Tax,map<string,Item> &Items);
  virtual int CompleteTransaction(PAYMENT_METHOD PM);
  mysqlpp::Result* LastResult;
  POS_ERRORS loginUser(string username, string password);
  POS_ERRORS logoutUser();

//debug output extras
#ifdef DEBUG
  const char* userOut(){return user.c_str();};
  const char* passOut(){return pass.c_str();};
#endif
};

#endif



