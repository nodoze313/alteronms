/***************************************************************************
 *   Copyright (C) 2008 by Phoenix Computer Management, LLC                *
 *   Written by James MacLachlan (james@pcmsite.net)                       *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 3 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/


#include "pos_gui.h"

pos_gui::pos_gui (KApplication* app, QWidget* parent, const char* name, WFlags fl )
		: Form1 ( parent,name,fl )
{
  mainApp = app;
  //set login status 
  login = true;
  //set all global settings
  qsDatabaseName = settings::databaseName();
  qsDatabaseURI = settings::databaseAddress();
  qsCompanyID = settings::companyID();
  qsCustomerID = settings::customerID();
  qsReceiptHeader = settings::receiptHeader();
  qsReceiptFooter = settings::receiptFooter();
  qsNumberCopiesCC = settings::numberCopiesCC();
  qsNumberCopiesCash = settings::numberCopiesCash();
  qsNumberCopiesCheck = settings::numberCopiesCheck();
  
  loadProgramableKeys();
  
  //start the main register class
  reg = new Register(qsDatabaseName.ascii(),qsDatabaseURI.ascii(),qsCompanyID.ascii(),qsCustomerID.ascii());
  
  //setup some addition gui stuff
  loginAsLabel = new QLabel( loginBox, "loginAsLabel" );
  //loginAsLabel->setGeometry( QRect( 130, 40, 180, 31 ) );
  loginAsLabel->setGeometry( QRect( 30, 20, 180, 31 ) );
  loginAsLabel->setText( tr2i18n( "Not Logged In" ) );
  loginAsLabel->setShown(false);
  
  //reprint handeling
  lastReceipt = new KTextEdit();
  
  //scanner sleep
  sl = new ScannerSleep(this);
  
  //set cursor to login
  loginDefaultCursorPosition();
  
  //current custom key
  currentCustomKey = 0;
}

pos_gui::~pos_gui()
{
}

/*$SPECIALIZATION$*/
void pos_gui::kTextEdit1_textChanged()
{
  if(isReady())
  {
    //check length
    //if == 12 activate return
    int para,index;
    kTextEdit1->getCursorPosition(&para,&index);
    QString upc = kTextEdit1->text(para);
    upc = upc.stripWhiteSpace();;
    if(upc.length() == 12)
    {
      //start the sleep function to get the item
      sl->start();
    };
    //put suggestion code here
  }
}

void pos_gui::kTextEdit1_3_textChanged()
{
}

void pos_gui::kTextEdit1_returnPressed()
{
  if(isReady())
  {
    //get previous line
    int para,index;
    kTextEdit1->getCursorPosition(&para,&index);
    QString upc = kTextEdit1->text(--para);
    upc = upc.stripWhiteSpace();
    //add to stack
    int amount = 1;
    if(inverseAdd->isOn())
    {
      amount = -1;
      inverseAdd->setOn(false);
    }
    POS_ERRORS e = reg->ManageItem(upc,amount);
    if(e == INVALID_UPC)
    {
      //qWarning("Ivalid UPC\n");
      posDisplay(QString("Invalid Item ID: " + upc));
    }
    else
    {
      posDisplay("");
    }
    //regenerate fields
    reg->CreateLists();
    setFields();
    //move the cursor to the UPC field and the next free line
    defaultCursorPosition();
  }
}

void pos_gui::kTextEdit1_clicked ( int,int )
{
}

void pos_gui::setFields()
{
  if(isReady())
  {
    //set data coloumns
    kTextEdit1->setText(reg->UPCList);
    kTextEdit1_2->setText(reg->DescriptionList);
    kTextEdit1_3->setText(reg->QuantityList);
    kTextEdit1_4->setText(reg->PriceList);
    kTextEdit1_5->setText(reg->TotalList);
  
    //set totals
    float fSubtotal=0.0,fTotal=0.0,fTax=0.0;
    reg->GetTotals(fSubtotal,fTax);
    fTotal = fSubtotal + fTax;
    subtotal->setText(ftoa(fSubtotal));
    tax->setText(ftoa(fTax));
    total->setText(ftoa(fTotal));
  }
}

void pos_gui::cancelButton_clicked()
{
  if(isReady())
  {
    reg->CancelSale();
    setFields();
    defaultCursorPosition();
  }
}

void pos_gui::kTextEdit1_3_clicked(int a,int b)
{
  //qtyClicked = true;
  //select the line
  int para,index;
  kTextEdit1_3->getCursorPosition(&para,&index);
  kTextEdit1_3->setSelection(para,0,para,kTextEdit1_3->paragraphLength(para));
}

void pos_gui::kTextEdit1_3_returnPressed()
{
  if(isReady())
  {
    int para,index;
    kTextEdit1_3->getCursorPosition(&para,&index);
    QString qty = kTextEdit1_3->text(--para);
    qty = qty.stripWhiteSpace();
    QString upc = kTextEdit1->text(para);
    upc = upc.stripWhiteSpace();
    reg->ManageItem(upc,qty.toInt(),SET);
    reg->CreateLists();
    setFields();
    //move the cursor to the UPC field and the next free line
    defaultCursorPosition();
  }
}

void pos_gui::posDisplay(QString toDisplay)
{
  info->setText(toDisplay);
}

void pos_gui::loginButton_clicked()
{
  //disable button so they can't click again
  loginButton->setEnabled(false);
  //if login button is at login state
  if(login)
  {
    //check to make sure both fields have data
    if(userEntry->text().length()<1 || passEntry->text().length()<1)
    {
      //enable button
      loginButton->setEnabled(true);
      //show not enough data
      posDisplay("Please give both a username and password.");
      loginDefaultCursorPosition();
      return;
    }
    //Attempt user login
    POS_ERRORS authSuccess = reg->UserLogin(userEntry->text(),passEntry->text());
    //success
    if(authSuccess == VALID_AUTH)
    {
      posDisplay("Login success");
      //show who is logged in
      QString qsLoginAs = "Logged in as user ";
      qsLoginAs += userEntry->text();
      loginAsLabel->setText(qsLoginAs);
      /*The old way of doing it
      now we remove the 4 components for the layout
      //hide box
      loginBox->setHidden(true);
      */
      //
      userLabel->setShown(false);
      passLabel->setShown(false);
      userEntry->setShown(false);
      passEntry->setShown(false);
      //show login as...
      loginAsLabel->setShown(true);
      
      //clear fields
      userEntry->clear();
      passEntry->clear();
      //change button to logout
      //loginButton->setText("Logout");
      //set state to logout
      login = false;
      //reenable button
      loginButton->setEnabled(true);
      //clear previous data user was messing with
      reg->CancelSale();
      setFields();
      defaultCursorPosition();
    }
    //failure
    else
    {
      posDisplay("Login failure");
      //clear fields
      userEntry->clear();
      passEntry->clear();
      //write failure to posDisplay
      posDisplay("Failed to log in!");
      //reenable button
      loginButton->setEnabled(true);
      loginDefaultCursorPosition();
    }
  }
  //if login button is not at login state
  else
  {
    //clear any existing sales
    reg->CancelSale();
    setFields();
    //log out user
    reg->UserLogout();
    //hide login as...
    loginAsLabel->setShown(false);
    /*the old way
    //show login box
    loginBox->setHidden(false);
    */
    //new way
    userLabel->setShown(true);
    passLabel->setShown(true);
    userEntry->setShown(true);
    passEntry->setShown(true);
    
    //set state to login
    login = true;
    //change button to login
    //loginButton->setText("Login");
    //reenable button
    loginButton->setEnabled(true);
    posDisplay("Logout success");
    loginDefaultCursorPosition();
  }
}

bool pos_gui::isReady()
{
  bool readyState = true;
  if(login)
  {
    posDisplay("Please login.");
    readyState = false;
  }
  return readyState;
}

bool pos_gui::isAdmin()
{
  QString* realAdminPass = new QString(settings::adminPasswordEdit());
  QCString* cRealAdminPass = new QCString(realAdminPass->utf8());
  delete realAdminPass;
  if(!cRealAdminPass->length())
  {
    delete cRealAdminPass;
    return true;
  }
    
  QCString* password = new QCString();
  int result = KPasswordDialog::getPassword(*password,i18n("Enter admin password please."));
  if(result == KPasswordDialog::Accepted)
  {
    if(*cRealAdminPass == *password)
    {
      delete cRealAdminPass;
      delete password;
      return true;
    }
  }
  //clear passwords
  delete cRealAdminPass;
  delete password;
  return false;
}

void pos_gui::calcChange()
{
  float Tender = tenderEdit->text().toFloat();
  float SubTotal, Tax, Total, Change;
  reg->GetTotals(SubTotal,Tax);
  Total = SubTotal + Tax;
  Change = Tender - Total;
  if(Change <= 0.00)
  {
    //changeEdit->setColor(QColor(255,84,58));
    //changeEdit->setBold(true);
  }
  else
  {
    //changeEdit->setColor(QColor(0,0,0));
    //changeEdit->setBold(false);
  }
  //changeEdit->setNum(Change);
}

void pos_gui::cashButton_clicked()
{
  if(isReady())
  {
    calcChange();
    //print receipt
    if(printReceipt(QString("Paid Cash"),qsNumberCopiesCash))
    {
      //clear screen
      reg->CompleteTransaction(CASH);
      setFields();
      defaultCursorPosition();
    }
  }
}

void pos_gui::creditButton_clicked()
{
  if(isReady())
  {
    calcChange();
    //print receipt
    if(printReceipt(QString("Paid by Credit Card"),qsNumberCopiesCC))
    {
      //clear screen
      reg->CompleteTransaction(CREDITCARD);
      setFields();
      defaultCursorPosition();
    }
  }
}

void pos_gui::checkButton_clicked()
{
  if(isReady())
  {
    calcChange();
    //print receipt
    if(printReceipt(QString("Paid by check"),qsNumberCopiesCheck))
    {
      //clear screen
      reg->CompleteTransaction(CHECK);
      setFields();
      defaultCursorPosition();
    }
  }
}

void pos_gui::passEntry_returnPressed(const QString&)
{
  //click the login button
  loginButton_clicked();
}
void pos_gui::userEntry_returnPressed(const QString&)
{
  //click the login button
  loginButton_clicked();
}

void pos_gui::settingsButton_clicked()
{
  if(isAdmin())
  {
    if(KConfigDialog::showDialog("settings"))
      return;
    KConfigDialog *dialog = new KConfigDialog(this, "settings", settings::self(),KDialogBase::IconList);
    dialog->enableButtonSeparator( true );
    General *general = new General(0, "General");
    Receipt *receipt = new Receipt(0,"Receipt");
    //Buttons *buttons = new Buttons(0,"Buttons");
    buttonModifiers *buttons = new buttonModifiers(0,"Buttons",0,this);
    ///todo: load programable keys into drop downs for appropriate tabs
    //tab 1
    QStringList tab1KeyList;
    tab1KeyList << "" << "Key 1" << "Key 2" << "Key 3" << "Key 4" << "Key 5" << "Key 6" << "Key 7" << "Key 8" << "Key 9" << "Key 10" << "Key 11" << "Key 12" << "Key 13" << "Key 14" << "Key 15";
    buttons->tab1ButtonDropDown->insertStringList(tab1KeyList);
    //tab 2
    QStringList tab2KeyList;
    tab1KeyList << "" << "Key 16" << "Key 17" << "Key 18" << "Key 19" << "Key 20" << "Key 21" << "Key 22" << "Key 23" << "Key 24" << "Key 25" << "Key 26" << "Key 27" << "Key 28" << "Key 29" << "Key 30";
    buttons->tab1ButtonDropDown->insertStringList(tab1KeyList);
    //tab 3
    QStringList tab3KeyList;
    tab1KeyList << "" << "Key 31" << "Key 32" << "Key 33" << "Key 34" << "Key 35" << "Key 36" << "Key 37" << "Key 38" << "Key 39" << "Key40" << "Key 41" << "Key 42" << "Key 43" << "Key 44" << "Key 45";
    buttons->tab1ButtonDropDown->insertStringList(tab1KeyList);
    dialog->addPage(general, i18n("General"), "configure");
    dialog->addPage(receipt, i18n("Receipt"), "fileprint");
    dialog->addPage(buttons, i18n("Buttons"), "key_bindings");
    connect(dialog,SIGNAL(settingsChanged()),this,SLOT(updateConfiguration()));
    dialog->show();
  }
}

void pos_gui::updateConfiguration()
{
  //update settings...
  qsDatabaseName = settings::databaseName();
  qsDatabaseURI = settings::databaseAddress();
  qsCompanyID = settings::companyID();
  qsCustomerID = settings::customerID();
  qsReceiptHeader = settings::receiptHeader();
  qsReceiptFooter = settings::receiptFooter();
  qsNumberCopiesCC = settings::numberCopiesCC();
  qsNumberCopiesCash = settings::numberCopiesCash();
  qsNumberCopiesCheck = settings::numberCopiesCheck();
  
  //these need to be saved and updated
  //programmable keys
  //tab 1
  //tab 2
  //tab 3
  ///todo: for each in map update settings
  
  //delete reg;
  //reg = new Register(qsDatabaseName.ascii(),qsDatabaseURI.ascii(),qsCompanyID.ascii(),qsCustomerID.ascii());
  
  //qsDefaultPrinter = ;
}

bool pos_gui::printReceipt(QString PaymentMethod,int count, bool useLast)
{
  //create a texteditor to pull from
  KTextEdit* pData = new KTextEdit();
  if(!useLast)
  {
    pData->insert(qsReceiptHeader);
    pData->insert(QString("\n\n"));
    pData->insert(reg->ItemsByRows);
    pData->insert(QString("\n------------------\n"));
    pData->insert(reg->SubTotalRow);
    pData->insert(QString("\n"));
    pData->insert(reg->TaxRow);
    pData->insert(QString("\n"));
    pData->insert(reg->TotalRow);
    pData->insert(QString("\n"));
    pData->insert(QString("Tendered: ") + tenderEdit->text());
    pData->insert(QString("\n"));
    pData->insert(QString("Change: ") + changeEdit->text());
    pData->insert(QString("\n"));
    pData->insert(QString("\n------------------\n"));
    pData->insert(PaymentMethod);
    pData->insert(QString("\n\n"));
    pData->insert(qsReceiptFooter);
    pData->insert(QString("\n"));
    lastReceipt = pData;
    lastReceiptPM = PaymentMethod;
  }
  else
  {
    pData = lastReceipt;
  }
//based on the print function from KEdit
  bool aborted = false;
  //QFont printFont = eframe->font();
  QFont printFont = pData->currentFont();
  QFont headerFont(printFont);
  headerFont.setBold(true);

  QFontMetrics printFontMetrics(printFont);
  QFontMetrics headerFontMetrics(headerFont);

  KPrinter *printer = new KPrinter;
  if(count)
  {
    if(printer->setup(this))
    {
      for(int ccount = 0;ccount < count;ccount++)
      {
        // set up KPrinter
        printer->setFullPage(false);
        printer->setCreator("Altero NMS POS");
    
        QPainter *p = new QPainter;
        p->begin( printer );
    
        QPaintDeviceMetrics metrics( printer );
    
        int dy = 0;
    
        p->setFont(headerFont);
        int w = printFontMetrics.width("M");
        p->setTabStops(8*w);
    
        int page = 1;
        int lineCount = 0;
        //int maxLineCount = eframe->numLines();
        int maxLineCount = pData->paragraphs();
        //qWarning(QString("Max Line count: ") + QString().setNum(maxLineCount));
    
        while(true)
        {
          dy = headerFontMetrics.lineSpacing();
          QRect body( 0, dy*2,  metrics.width(), metrics.height()-dy*2);
    
          //p->drawText(0, 0, metrics.width(), dy, Qt::AlignLeft, qsReceiptHeader);
    
          //QPen pen;
          //pen.setWidth(3);
          //p->setPen(pen);
    
          //p->drawLine(0, dy+dy/2, metrics.width(), dy+dy/2);
    
          int y = dy*2;
          while(lineCount < maxLineCount) 
          {
            //QString text = eframe->textLine(lineCount);
            QString text = pData->text(lineCount);
            if( text.isEmpty() )
              text = " ";	// don't ignore empty lines
            QRect r = p->boundingRect(0, y, body.width(), body.height(), QPainter::ExpandTabs | QPainter::WordBreak, text);
    
            dy = r.height();
    
            if (y+dy > metrics.height()) break;
    
            p->drawText(0, y, metrics.width(), metrics.height() - y, QPainter::ExpandTabs | QPainter::WordBreak, text);
            //qWarning(QString("Current Line: ") + QString().setNum(lineCount) + text);
            y += dy;
            lineCount++;
          }
          //qWarning(QString("Line count: ") + QString().setNum(lineCount));
          if (lineCount >= maxLineCount)
            break;
    
          printer->newPage();
          page++;
        }
    
        p->end();
        delete p;
      }
    }
    else
    {
      return false;
    }
  }
  delete printer;
  if (aborted)
    posDisplay(i18n("Printing aborted."));
  else
    posDisplay(i18n("Printing complete."));
  return true;
}

void pos_gui::defaultCursorPosition()
{
  int line = reg->LineItems.size();
  kTextEdit1->setFocus();
  kTextEdit1->setCursorPosition(line,0);
}

void pos_gui::loginDefaultCursorPosition()
{
  userEntry->setFocus();
}

void pos_gui::fullScreenButton_clicked()
{
  if(this->windowState() & WindowFullScreen)
  {
    if(settings::fullscreenAdminOptionEdit())
    {
      if(isAdmin())
      {
        QPixmap buttonsImg;
        buttonsImg.loadFromData( img_fullscreen, sizeof( img_fullscreen ), "PNG" );
        fullScreenButton->setPixmap(buttonsImg);
        this->setWindowState(this->windowState() ^ WindowFullScreen);
      }
    }
    else
    {
      QPixmap buttonsImg;
      buttonsImg.loadFromData( img_fullscreen, sizeof( img_fullscreen ), "PNG" );
      fullScreenButton->setPixmap(buttonsImg);
      this->setWindowState(this->windowState() ^ WindowFullScreen);
    }
  }
  else
  {
    QPixmap buttonsImg;
    buttonsImg.loadFromData( img_nofullscreen, sizeof( img_nofullscreen ), "PNG" );
    fullScreenButton->setPixmap(buttonsImg);
    this->setWindowState(WindowFullScreen);
  }
  //this->setWindowState(this->windowState() ^ WindowFullScreen);
}

void pos_gui::ReprintButton_clicked()
{
  if(lastReceipt->length())
  {
    printReceipt(lastReceiptPM,1,true);
  }
  defaultCursorPosition();
}

QWidget* pos_gui::findFocus()
{
  if(kTextEdit1->hasFocus())
  {
    return kTextEdit1;
  }
  return 0;
}

void pos_gui::oneKey_clicked()
{
  QKeyEvent* e = new QKeyEvent(QEvent::KeyPress,Key_1,0x31,0x0,"1");
  softKPWrite(e);
}

void pos_gui::twoKey_clicked()
{
  QKeyEvent* e = new QKeyEvent(QEvent::KeyPress,Key_2,0x32,0x0,"2");
  softKPWrite(e);
}

void pos_gui::threeKey_clicked()
{
  QKeyEvent* e = new QKeyEvent(QEvent::KeyPress,Key_3,0x33,0x0,"3");
  softKPWrite(e);
}

void pos_gui::fourKey_clicked()
{
  QKeyEvent* e = new QKeyEvent(QEvent::KeyPress,Key_4,0x34,0x0,"4");
  softKPWrite(e);
}

void pos_gui::fiveKey_clicked()
{
  QKeyEvent* e = new QKeyEvent(QEvent::KeyPress,Key_5,0x35,0x0,"5");
  softKPWrite(e);
}

void pos_gui::sixKey_clicked()
{
  QKeyEvent* e = new QKeyEvent(QEvent::KeyPress,Key_6,0x36,0x0,"6");
  softKPWrite(e);
}

void pos_gui::sevenKey_clicked()
{
  QKeyEvent* e = new QKeyEvent(QEvent::KeyPress,Key_1,0x37,0x0,"7");
  softKPWrite(e);
}

void pos_gui::eightKey_clicked()
{
  QKeyEvent* e = new QKeyEvent(QEvent::KeyPress,Key_1,0x38,0x0,"8");
  softKPWrite(e);
}

void pos_gui::nineKey_clicked()
{
  QKeyEvent* e = new QKeyEvent(QEvent::KeyPress,Key_1,0x39,0x0,"9");
  softKPWrite(e);
}

void pos_gui::enterKey_clicked()
{
  QKeyEvent* e = new QKeyEvent(QEvent::KeyPress,Key_Enter,0x0A,0x0);
  softKPWrite(e);
}

void pos_gui::clrKey_clicked()
{
  QKeyEvent* e = new QKeyEvent(QEvent::KeyPress,Key_Backspace,0x8,0x0);
  softKPWrite(e);
}

void pos_gui::zeroKey_clicked()
{
  QKeyEvent* e = new QKeyEvent(QEvent::KeyPress,Key_0,0x30,0x0,"0");
  softKPWrite(e);
}

void pos_gui::zerozeroKey_clicked()
{
  QKeyEvent* e = new QKeyEvent(QEvent::KeyPress,Key_0,0x30,0x0,"0");
  QKeyEvent* f = new QKeyEvent(QEvent::KeyPress,Key_0,0x30,0x0,"0");
  softKPWrite(e);
  softKPWrite(f);
}

void pos_gui::decimalKey_clicked()
{
  QKeyEvent* e = new QKeyEvent(QEvent::KeyPress,Key_Period,0x2e,0x0,".");
  softKPWrite(e);
}

void pos_gui::tenderEdit_returnPressed()
{
  tenderTotal = tenderEdit->text();
}


void pos_gui::softKPWrite(QKeyEvent* e)
{
  QWidget* f = findFocus();
  if(f)
  {
    mainApp->postEvent(f,e);
  }
  else
  {
    if(tenderEdit->hasFocus())
    {
      mainApp->postEvent(tenderEdit,e);
    }
    if(kTextEdit1_3->hasFocus())
    {
      mainApp->postEvent(kTextEdit1_3,e);
    }
  }
}

void pos_gui::programableKeyPushed(int KeyID)
{
  //call from the map here to the proper CustomKey
  if(isReady())
  {
    CustomKey tCK = customKeys.find(KeyID)->second;
    QString itemID = tCK.getItemID();
    QString qty = tCK.getQuantity();
    if(itemID.length() && qty.length())
    {
      reg->ManageItem(itemID,qty.toInt());
      reg->CreateLists();
      setFields();
      //move the cursor to the UPC field and the next free line
      defaultCursorPosition();
    }
  }
}

void pos_gui::loadProgramableKeys()
{
  
  //programmable keys
  //tab 1
  settings* s = settings::self();
  customKeys.insert(make_pair(1,CustomKey(programableKey1,"1",s)));
  customKeys.insert(make_pair(2,CustomKey(programableKey2,"2",s)));
  customKeys.insert(make_pair(3,CustomKey(programableKey3,"3",s)));
  customKeys.insert(make_pair(4,CustomKey(programableKey4,"4",s)));
  customKeys.insert(make_pair(5,CustomKey(programableKey5,"5",s)));
  customKeys.insert(make_pair(6,CustomKey(programableKey6,"6",s)));
  customKeys.insert(make_pair(7,CustomKey(programableKey7,"7",s)));
  customKeys.insert(make_pair(8,CustomKey(programableKey8,"8",s)));
  customKeys.insert(make_pair(9,CustomKey(programableKey9,"9",s)));
  customKeys.insert(make_pair(10,CustomKey(programableKey10,"10",s)));
  customKeys.insert(make_pair(11,CustomKey(programableKey11,"11",s)));
  customKeys.insert(make_pair(12,CustomKey(programableKey12,"12",s)));
  customKeys.insert(make_pair(13,CustomKey(programableKey13,"13",s)));
  customKeys.insert(make_pair(14,CustomKey(programableKey14,"14",s)));
  customKeys.insert(make_pair(15,CustomKey(programableKey15,"15",s)));
  //tab 2
  customKeys.insert(make_pair(16,CustomKey(programableKey16,"6",s)));
  customKeys.insert(make_pair(17,CustomKey(programableKey17,"7",s)));
  customKeys.insert(make_pair(18,CustomKey(programableKey18,"8",s)));
  customKeys.insert(make_pair(19,CustomKey(programableKey19,"9",s)));
  customKeys.insert(make_pair(20,CustomKey(programableKey20,"10",s)));
  customKeys.insert(make_pair(21,CustomKey(programableKey21,"21",s)));
  customKeys.insert(make_pair(22,CustomKey(programableKey22,"22",s)));
  customKeys.insert(make_pair(23,CustomKey(programableKey23,"23",s)));
  customKeys.insert(make_pair(24,CustomKey(programableKey24,"24",s)));
  customKeys.insert(make_pair(25,CustomKey(programableKey25,"25",s)));
  customKeys.insert(make_pair(26,CustomKey(programableKey26,"26",s)));
  customKeys.insert(make_pair(27,CustomKey(programableKey27,"27",s)));
  customKeys.insert(make_pair(28,CustomKey(programableKey28,"28",s)));
  customKeys.insert(make_pair(29,CustomKey(programableKey29,"29",s)));
  customKeys.insert(make_pair(30,CustomKey(programableKey30,"30",s)));
  //tab 3
  customKeys.insert(make_pair(31,CustomKey(programableKey31,"31",s)));
  customKeys.insert(make_pair(32,CustomKey(programableKey32,"32",s)));
  customKeys.insert(make_pair(33,CustomKey(programableKey33,"33",s)));
  customKeys.insert(make_pair(34,CustomKey(programableKey34,"34",s)));
  customKeys.insert(make_pair(35,CustomKey(programableKey35,"35",s)));
  customKeys.insert(make_pair(36,CustomKey(programableKey36,"36",s)));
  customKeys.insert(make_pair(37,CustomKey(programableKey37,"37",s)));
  customKeys.insert(make_pair(38,CustomKey(programableKey38,"38",s)));
  customKeys.insert(make_pair(39,CustomKey(programableKey39,"39",s)));
  customKeys.insert(make_pair(40,CustomKey(programableKey40,"40",s)));
  customKeys.insert(make_pair(41,CustomKey(programableKey41,"41",s)));
  customKeys.insert(make_pair(42,CustomKey(programableKey42,"42",s)));
  customKeys.insert(make_pair(43,CustomKey(programableKey43,"43",s)));
  customKeys.insert(make_pair(44,CustomKey(programableKey44,"44",s)));
  customKeys.insert(make_pair(45,CustomKey(programableKey45,"45",s)));
}

void pos_gui::programableKey1_clicked()
{
  programableKeyPushed(1);
}

void pos_gui::programableKey2_clicked()
{
  programableKeyPushed(2);
}

void pos_gui::programableKey3_clicked()
{
  programableKeyPushed(3);
}

void pos_gui::programableKey4_clicked()
{
  programableKeyPushed(4);
}

void pos_gui::programableKey5_clicked()
{
  programableKeyPushed(5);
}

void pos_gui::programableKey6_clicked()
{
  programableKeyPushed(6);
}

void pos_gui::programableKey7_clicked()
{
  programableKeyPushed(7);
}

void pos_gui::programableKey8_clicked()
{
  programableKeyPushed(8);
}

void pos_gui::programableKey9_clicked()
{
  programableKeyPushed(9);
}

void pos_gui::programableKey10_clicked()
{
  programableKeyPushed(10);
}

void pos_gui::programableKey11_clicked()
{
  programableKeyPushed(11);
}

void pos_gui::programableKey12_clicked()
{
  programableKeyPushed(12);
}

void pos_gui::programableKey13_clicked()
{
  programableKeyPushed(13);
}

void pos_gui::programableKey14_clicked()
{
  programableKeyPushed(14);
}

void pos_gui::programableKey15_clicked()
{
  programableKeyPushed(15);
}

void pos_gui::programableKey16_clicked()
{
  programableKeyPushed(16);
}

void pos_gui::programableKey17_clicked()
{
  programableKeyPushed(17);
}

void pos_gui::programableKey18_clicked()
{
  programableKeyPushed(18);
}

void pos_gui::programableKey19_clicked()
{
  programableKeyPushed(19);
}

void pos_gui::programableKey20_clicked()
{
  programableKeyPushed(20);
}

void pos_gui::programableKey21_clicked()
{
  programableKeyPushed(21);
}

void pos_gui::programableKey22_clicked()
{
  programableKeyPushed(22);
}

void pos_gui::programableKey23_clicked()
{
  programableKeyPushed(23);
}

void pos_gui::programableKey24_clicked()
{
  programableKeyPushed(24);
}

void pos_gui::programableKey25_clicked()
{
  programableKeyPushed(25);
}

void pos_gui::programableKey26_clicked()
{
  programableKeyPushed(26);
}

void pos_gui::programableKey27_clicked()
{
  programableKeyPushed(27);
}

void pos_gui::programableKey28_clicked()
{
  programableKeyPushed(28);
}

void pos_gui::programableKey29_clicked()
{
  programableKeyPushed(29);
}

void pos_gui::programableKey30_clicked()
{
  programableKeyPushed(30);
}

void pos_gui::programableKey31_clicked()
{
  programableKeyPushed(31);
}

void pos_gui::programableKey32_clicked()
{
  programableKeyPushed(32);
}

void pos_gui::programableKey33_clicked()
{
  programableKeyPushed(33);
}

void pos_gui::programableKey34_clicked()
{
  programableKeyPushed(34);
}

void pos_gui::programableKey35_clicked()
{
  programableKeyPushed(35);
}

void pos_gui::programableKey36_clicked()
{
  programableKeyPushed(36);
}

void pos_gui::programableKey37_clicked()
{
  programableKeyPushed(37);
}

void pos_gui::programableKey38_clicked()
{
  programableKeyPushed(38);
}

void pos_gui::programableKey39_clicked()
{
  programableKeyPushed(39);
}

void pos_gui::programableKey40_clicked()
{
  programableKeyPushed(40);
}

void pos_gui::programableKey41_clicked()
{
  programableKeyPushed(41);
}

void pos_gui::programableKey42_clicked()
{
  programableKeyPushed(42);
}

void pos_gui::programableKey43_clicked()
{
  programableKeyPushed(43);
}

void pos_gui::programableKey44_clicked()
{
  programableKeyPushed(44);
}

void pos_gui::programableKey45_clicked()
{
  programableKeyPushed(45);
}

/*

void pos_gui::tab1ButtonDropDown_activated(int index)
{
  //note index starts at zero our keys are named 1 - 45
  //change the key we are saving to
  if(index)
  {
    currentCustomKey = &customKeys.find(index)->second;
    //load the data for that key in to the fields
    mButtons->tab1TitleEdit->setText(currentCustomKey->getTitle());
    mButtons->qtyEdit1->setText(currentCustomKey->getQuantity());
    mButtons->itemCodeEdit1->setText(currentCustomKey->getItemID());
  }
  else
  {
    currentCustomKey = 0;
  }
}

void pos_gui::tab2ButtonDropDown_activated(int index)
{
  //note index starts at zero our keys are named 1 - 45
  //change the key we are saving to
  if(index)
  {
    currentCustomKey = &customKeys.find(index + 15)->second;
    //load the data for that key in to the fields
    mButtons->tab2TitleEdit->setText(currentCustomKey->getTitle());
    mButtons->qtyEdit2->setText(currentCustomKey->getQuantity());
    mButtons->itemCodeEdit2->setText(currentCustomKey->getItemID());
  }
  else
  {
    currentCustomKey = 0;
  }
}

void pos_gui::tab3ButtonDropDown_activated(int index)
{
  //note index starts at zero our keys are named 1 - 45
  //change the key we are saving to
  if(index)
  {
    currentCustomKey = &customKeys.find(index + 30)->second;
    //load the data for that key in to the fields
    mButtons->tab3TitleEdit->setText(currentCustomKey->getTitle());
    mButtons->qtyEdit3->setText(currentCustomKey->getQuantity());
    mButtons->itemCodeEdit3->setText(currentCustomKey->getItemID());
  }
  else
  {
    currentCustomKey = 0;
  }
}
*/


//
ScannerSleep::ScannerSleep(pos_gui* pg)
{
  PG = pg;
}

void ScannerSleep::run()
{
  usleep(100000);
  //set an event
  QKeyEvent* e = new QKeyEvent(QEvent::KeyPress,Key_Enter,0x0A,0x0);
  PG->mainApp->postEvent(PG->kTextEdit1,e);
}

QString CustomKey::getItemID()
{
  return ItemID->property().toString();
}
void CustomKey::setItemID(QString itemID)
{
  ItemID->setProperty(QVariant(itemID));
}

QString CustomKey::getQuantity()
{
  return Quantity->property().toString();
}
void CustomKey::setQuantity(QString quantity)
{
  Quantity->setProperty(QVariant(quantity));
}

QString CustomKey::getTitle()
{
  return Title->property().toString();
}
void CustomKey::setTitle(QString title)
{
  Title->setProperty(QVariant(title));
  Button->setText(Title->property().toString());
}

CustomKey::CustomKey(KPushButton* button,QString buttonID,settings* globalSettings)
{
  Button = button;
  ItemID = globalSettings->findItem(QString("CustomKeysItemIDEdit") + buttonID);
  Quantity = globalSettings->findItem(QString("CustomKeysCountEdit") + buttonID);
  Title = globalSettings->findItem(QString("CustomKeysNameEdit") + buttonID);
}
/*

void pos_gui::tab1TitleEdit_textChanged(const QString &title)
{
  //update the current button title
  currentCustomKey->setTitle(title);
}

void pos_gui::itemCodeEdit1_textChanged(const QString &itemID)
{
  currentCustomKey->setItemID(itemID);
}

void pos_gui::qtyEdit1_textChanged(const QString &quantity)
{
  currentCustomKey->setQuantity(quantity);
}

void pos_gui::tab2TitleEdit_textChanged(const QString &title)
{
  currentCustomKey->setTitle(title);
}

void pos_gui::itemCodeEdit2_textChanged(const QString &itemID)
{
  currentCustomKey->setItemID(itemID);
}

void pos_gui::qtyEdit2_textChanged(const QString &quantity)
{
  currentCustomKey->setQuantity(quantity);
}

void pos_gui::tab3TitleEdit_textChanged(const QString &title)
{
  currentCustomKey->setTitle(title);
}

void pos_gui::itemCodeEdit3_textChanged(const QString &itemID)
{
  currentCustomKey->setItemID(itemID);
}

void pos_gui::qtyEdit3_textChanged(const QString &quantity)
{
  currentCustomKey->setQuantity(quantity);
}

*/







#include "pos_gui.moc"

