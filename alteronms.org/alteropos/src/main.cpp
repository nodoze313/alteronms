/***************************************************************************
 *   Copyright (C) 2008 by James MacLachlan   *
 *   james@pcmsite.net   *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 3 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/


#include "pos_gui.h"

#include <kapplication.h>
#include <kaboutdata.h>
#include <kcmdlineargs.h>
#include <klocale.h>

static const char description[] = I18N_NOOP("Altero Point of Sale");

static const char version[] = "0.1";

static KCmdLineOptions options[] =
{
//{ "+[URL]", I18N_NOOP( "Document to open" ), 0 },
  KCmdLineLastOption
};

int main(int argc, char **argv)
{
  KAboutData about("Altero POS", I18N_NOOP("Altero POS"), version, description,
                    KAboutData::License_GPL, "(C) 2008 Altero, LLC", 0, 0, "james@pcmsite.net");
  about.addAuthor( "James MacLachlan", 0, "james@pcmsite.net" );
  KCmdLineArgs::init(argc, argv, &about);
  KCmdLineArgs::addCmdLineOptions( options );
  KApplication app;
  pos_gui *mainWin = 0;

  // no session.. just start up normally
  KCmdLineArgs *args = KCmdLineArgs::parsedArgs();

  /// @todo do something with the command line args here

  mainWin = new pos_gui(&app);
  app.setMainWidget( mainWin );
  mainWin->show();

  args->clear();

  //set the tab order
  mainWin->setTabOrder(mainWin->userEntry,mainWin->passEntry);
  mainWin->setTabOrder(mainWin->passEntry,mainWin->loginButton);

  // mainWin has WDestructiveClose flag by default, so it will delete itself.
  return app.exec();
}

